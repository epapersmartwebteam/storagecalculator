﻿var azure = require('azure-storage');
var azureAccountName = 'myxteam';
var azureAccessKey = 'qNNnIalz3ySNnqfmOHt4XGsx4U/kyq/5YWPA/+dkzOPAceOfQC7Spq6z+Bv9AoBNJyqZA+ktJL7sxuYdZG1SHQ==';
var attachmentContainer = "attach";
var chatContainer = 'chat';
exports.attachmentContainer = attachmentContainer;
exports.chatContainer = chatContainer;

function getBlobSizeWithPrefix(containerName, prefix, callback) {
    var blobService = azure.createBlobService(azureAccountName, azureAccessKey);
    blobService.listBlobsSegmentedWithPrefix(containerName, prefix, null, function (error, result, response) {
        if (!error) {
            var size = 0;
            result.entries.forEach(function (item) {
                size += parseInt(item.contentLength);
            });
            callback(size);
        }
        else {
            console.log(error);
            callback(0);
        }
    });
}

exports.getBlobSizeWithPrefix = getBlobSizeWithPrefix;

function deleteBlob(containerName, itemName, callback) {
    var blobService = azure.createBlobService(azureAccountName, azureAccessKey);
    blobService.deleteBlob(containerName, itemName, function (error, response) {
        if (!error) {
            // Blob has been deleted
        }        
        callback(error);
    });
}

exports.deleteBlob = deleteBlob;