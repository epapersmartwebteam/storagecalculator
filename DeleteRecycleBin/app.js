var async = require('async');
var _ = require('lodash');
var scheduler = require('node-schedule');
var moment = require('moment');

var db = require('./db');
var azureStorage = require('./azureStorage');
var log = require('./log');

var calculateJobCron = '0 0 5 * * *'; //5h UTC, 12h ?�m VN

var expireDays = 60;
var itemCount = 100;
var asyncTaskCount = 10;
function getTheFiles(lastItemId) {
    db.getRecycleFileExpire(expireDays, itemCount, lastItemId, (err, lst) => {
        if (!err) {
            async.eachLimit(lst, asyncTaskCount, (item, cb) => {
                log.info('File: ' + item.FileId + ' - ' + moment(item.DeleteDate).format('YYYY-MM-DD HH:mm') + ' - ' + item.Link);
                if (item.CloudStorageId == 4) {
                    if (item.Link) {
                        var blobName = item.Link.replace(azureStorage.attachmentContainer + '/', '');
                        azureStorage.deleteBlob(azureStorage.attachmentContainer, blobName, (err) => {
                            if (err) {
                                log.error(err);
                            }
                            db.deleteAttachFile(item.FileId, (e) => {
                                cb();
                            });
                        });
                    }
                }
                else {
                    db.deleteAttachFile(item.FileId, (e) => {
                        cb();
                    });
                }
            }, (err) => {
                if (lst.length == itemCount) {
                    log.info('get next list');
                    getTheFiles(lst[lst.length - 1].FileId);
                }
            });
        }
    });
}

setTimeout(() => {
    console.log('Run at cron: ' + calculateJobCron);
    scheduler.scheduleJob(calculateJobCron, function (fireDate) {
        log.info('Empty Recycle Bin was supposed to run at ' + fireDate + ', but actually ran at ' + new Date());

        getTheFiles(null);
    });
}, 2000);