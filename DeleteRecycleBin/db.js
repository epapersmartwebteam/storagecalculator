﻿var sql = require('mssql');
var moment = require('moment');
var config = {
    user: 'adminsrv',
    password: 'P@12345678vm',
    server: '119.82.135.46', // You can use 'localhost\\instance' to connect to named instance
    database: 'myxteam'
};

const connection = new sql.ConnectionPool(config, err => {
    console.log(err);
});

connection.on('error', function (err) {
    console.log('connection error');
    console.log(err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});

function getRecycleFileExpire(expireDay, itemCount, lastItemId, callback) {

    var request = new sql.Request(connection);

    var expireDate = moment().utc().add(-expireDay, "days").unix();

    var query = 'Select top ' + itemCount + ' * ';
    query += ' From AttachFiles ';
    query += ' Where RecycleBin = 1 and DeleteDateUnix < ' + expireDate;
    if (lastItemId) {
        query += ' and FileId > ' + lastItemId;
    }
    query += ' Order by FileId ';
    request.query(query, (err, result) => {
        if (err) {
            callback(err);
        }
        else {
            callback(null, result.recordset);
        }
    });
}

exports.getRecycleFileExpire = getRecycleFileExpire;

function deleteAttachFile(fileId, callback) {
    var request = new sql.Request(connection);
    request.input('FileId', sql.BigInt, fileId);
    request.execute('deleteAttachFile', (err, result) => {
        callback(err);
    });
}

exports.deleteAttachFile = deleteAttachFile;