var async = require('async');
var _ = require('lodash');
var scheduler = require('node-schedule');

var calculateJobCron = '50 13 10 * * 4,7';

var db = require('./db');
var azureStorage = require('./azureStorage');
var log = require('./log');

var itemCount = 100;
var asyncGetSize = 100;

function getTheUsers(itemCount, lastUserId) {
    db.getUsers(itemCount, lastUserId, (err, lstUser) => {        

        async.eachSeries(lstUser, (user, cb) => {
            var totalSize = 0;
            db.getAllTeamProjectOfUser(user.UserId, (err, lstProject) => {
                //console.warn(user.UserId);
                //console.log(lstProject);

                async.eachLimit(lstProject, asyncGetSize, (project, cbx) => {
                    azureStorage.getBlobSizeWithPrefix(azureStorage.attachmentContainer, project.ProjectId + '/', (size) => {
                        totalSize += size;
                        cbx();
                    });
                }, (errX) => {

                    db.getAllTeamChatRoomOfUser(user.UserId, (errC, lstChat) => {
                        if (lstChat && lstChat.length > 0) {
                            async.eachLimit(lstChat, asyncGetSize, (chat, cbC) => {
                                azureStorage.getBlobSizeWithPrefix(azureStorage.chatContainer, chat.ChatRoomId + '/', (size) => {
                                    totalSize += size;
                                    cbC();
                                });
                            },
                                (errCX) => {
                                    totalSize = parseInt(totalSize / (1024 * 1024));
                                    log.info(user.UserId + ' : ' + totalSize.toLocaleString('vi') + ' MB');
                                    db.updateUserUsedStorage(user.UserId, totalSize, () => {
                                        cb();
                                    });
                                });
                        }
                        else {
                            totalSize = parseInt(totalSize / (1024 * 1024));
                            log.info(user.UserId + ' : ' + totalSize.toLocaleString('vi') + ' MB');
                            db.updateUserUsedStorage(user.UserId, totalSize, () => {
                                cb();
                            });
                        }
                    });
                });
            });
        }, (err) => {
            log.info(lstUser.length + ' users done, get next list');
            if (lstUser.length == itemCount) {
                //log.error(lstUser[itemCount - 1].UserId);
                getTheUsers(itemCount, lstUser[itemCount - 1].UserId);
            }
        });
    });
}


setTimeout(() => {
    scheduler.scheduleJob(calculateJobCron, function (fireDate) {
        log.info('Storage Calculator was supposed to run at ' + fireDate + ', but actually ran at ' + new Date());

        getTheUsers(itemCount, 46341);
    });
}, 1000);