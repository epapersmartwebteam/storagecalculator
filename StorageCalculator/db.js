﻿var sql = require('mssql');

var config = {
    user: 'adminsrv',
    password: 'P@12345678vm',
    server: '119.82.135.46', // You can use 'localhost\\instance' to connect to named instance
    database: 'myxteam'
};

const connection = new sql.ConnectionPool(config, err => {
});

connection.on('error', function (err) {
    console.log('connection error');
    console.log(err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});

function getUsers(itemCount, lastItemId, callback) {
    var request = new sql.Request(connection);
    var query = 'Select top ' + itemCount + ' UserId From Users';
    if (lastItemId) {
        query += ' where UserId >' + lastItemId;
    }
    request.query(query, (err, result) => {
        if (err) {
            callback(err);
        }
        else {
            callback(null, result.recordset);
        }        
    });
}
exports.getUsers = getUsers;

function getAllTeamProjectOfUser(userId, callback) {
    var request = new sql.Request(connection);
    var query = 'Select ProjectId From Projects P inner join Workspace W on P.WorkspaceId = W.WorkspaceId Where W.OwnerId = ' + userId;
    request.query(query, (err, result) => {
        if (err) {
            callback(err);
        }
        else {
            callback(null, result.recordset);
        }
    });
}
exports.getAllTeamProjectOfUser = getAllTeamProjectOfUser;

function getAllTeamChatRoomOfUser(userId, callback) {
    var request = new sql.Request(connection);
    request.input('UserId', sql.BigInt, userId);
    request.execute('getAllTeamChatRoomOfUser', (err, result) => {
        if (err) {
            callback(err);
        }
        else {
            callback(null, result.recordset);
        }
    });
}
exports.getAllTeamChatRoomOfUser = getAllTeamChatRoomOfUser;

function updateUserUsedStorage(userId, usedStorage, callback) {
    var request = new sql.Request(connection);
    var query = 'Update UserPlans Set UsedStorage = ' + usedStorage + ' Where UserId = ' + userId;
    request.query(query, (err, result) => {
        if (err) {
            callback(err);
        }
        else {
            callback(null);
        }
    });
}
exports.updateUserUsedStorage = updateUserUsedStorage;

