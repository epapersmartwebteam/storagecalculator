﻿var _ = require('lodash');
var config = require('./config.js');
var sql = require('mssql');
var helper = require('./helper')();

var connection = new sql.ConnectionPool(config.dbConnection, err => {
    //console.log(err);
});

connection.on('error', function (err) {
    console.log('connection error');
    console.log(err);
    connection = new sql.ConnectionPool(config.dbConnection);
    connection.connect();
});

module.exports = () => {
    var getUserNotLoginOverNDays = function (days, lastId, topN, callback) {
        var request = new sql.Request(connection);
        request.input('Days', sql.Int, days);
        request.input('LastId', sql.BigInt, lastId);
        request.input('TopN', sql.Int, topN);

        console.log(days, lastId, topN);

        var query = 'getUserNotLoginOverNDayUnix';

        request.execute(query, (err, result) => {
            console.log(err);
            if (err) {
                callback(err);
            }
            else {
                console.log(result);
                callback(null, result.recordset);
            }
        });
    };

    var getUserInActiveOverNDays = function (days, lastId, topN, callback) {
        var request = new sql.Request(connection);
        request.input('Days', sql.Int, days);
        request.input('LastId', sql.BigInt, lastId);
        request.input('TopN', sql.Int, topN);

        var query = 'getUserInActiveOverNDayUnix';

        request.execute(query, (err, result) => {
            console.log(err);
            if (err) {
                callback(err);
            }
            else {
                console.log(result);
                callback(null, result.recordset);
            }
        });
    };

    var checkUserIsLoginAndDeleteFromQueue = function (userId, days, callback) {
        var request = new sql.Request(connection);
        request.input('Days', sql.Int, days);
        request.input('UserId', sql.BigInt, userId);

        var query = "checkUserIsLoginAndDeleteFromQueue";
        request.execute(query, (err, result) => {
            console.log(err);
            if (err) {
                callback(true);
            }
            else {
                let canDelete = true;
                
                if (result && result.recordset.length > 0) {
                    canDelete = result.recordset[0].CanDelete;
                    console.log('canDelete', canDelete);
                }

                callback(canDelete);
            }
        });
    }

    var addDeleteQueueUsers = function (lstUser, isInActive, callback) {
        const table = new sql.Table('DeleteQueueUsers');
        table.create = true;
        table.columns.add('UserId', sql.BigInt, { nullable: false, primary: true });
        table.columns.add('CreateDate', sql.BigInt, { nullable: true });
        table.columns.add('ContactId', sql.BigInt, { nullable: true });
        table.columns.add('Resent', sql.Bit, { nullable: true });
        table.columns.add("IsInActive", sql.Bit, { nullable: true });

        var createDate = helper.getUnixTimeStamp();

        _.each(lstUser, (user) => {
            table.rows.add(user.userId, createDate, user.contactId, false, isInActive);
        });

        var request = new sql.Request(connection);

        request.bulk(table, (err, result) => {
            if (err) {
                callback(err);
            }
            else {
                callback(null, result.recordset);
            }
        });
    };

    var getDeleteQueueUsers = function (isInActive, days, lastId, itemCount, isResend, callback) {
        var request = new sql.Request(connection);
        var deleteDate = helper.getUnixTimeStamp() - helper.convertDayToSecond(days);
        console.log('deleteDate', deleteDate);

        request.input('DeleteDate', sql.BigInt, deleteDate);
        request.input('IsInActive', sql.Bit, isInActive);
        request.input('LastId', sql.BigInt, lastId);


        var query = 'Select ';
        if (itemCount && itemCount > 0) {
            query += ' top ' + itemCount;
        }
        query += ' * From DeleteQueueUsers Where CreateDate <= @DeleteDate and IsInActive = @IsInActive ';
        query += ' and (IsDeleted is null or IsDeleted = 0) ';
        if (lastId) {
            query += ' and UserId > @LastId ';
        }

        if (isResend != null) {
            if (isResend === false) {
                query += ' and (Resent = 0 or Resent is null) ';
            }
            else {
                query += ' and Resent = 1 ';
            }
        }

        query += ' Order by UserId ';

        request.query(query, (err, result) => {
            if (err) {
                callback(err);
            }
            else {
                
                callback(null, result.recordset);
            }
        });
    };

    var removeDeleteQueueUsers = function (lstUserId, callback) {
        var request = new sql.Request(connection);

        var query = 'Delete From DeleteQueueUsers Where UserId in (' + lstUserId.toString() + ')';
        request.query(query, (err, result) => {
            if (err) {
                callback(err);
            }
            else {
                callback(null);
            }
        });
    };

    var removeDeleteQueueUser = function (userId, callback) {
        var request = new sql.Request(connection);
        request.input('UserId', sql.BigInt, userId);

        var query = 'Delete From DeleteQueueUsers Where UserId = @UserId';

        request.query(query, (err, result) => {
            if (err) {
                callback(err);
            }
            else {
                callback(null);
            }
        });
    };

    var updateDeleteQueueResend = function (lstUserId, callback) {
        var request = new sql.Request(connection);

        var query = 'Update DeleteQueueUsers ';
        query += ' Set Resent = 1 ';
        query += ' Where UserId in (' + lstUserId.toString() + ')';
        request.query(query, (err, result) => {
            if (err) {
                callback(err);
            }
            else {
                callback(null);
            }
        });
    };

    return {
        getUserNotLoginOverNDays: getUserNotLoginOverNDays,
        getUserInActiveOverNDays: getUserInActiveOverNDays,
        addDeleteQueueUsers: addDeleteQueueUsers,
        getDeleteQueueUsers: getDeleteQueueUsers,
        removeDeleteQueueUsers: removeDeleteQueueUsers,
        removeDeleteQueueUser: removeDeleteQueueUser,
        updateDeleteQueueResend: updateDeleteQueueResend,
        checkUserIsLoginAndDeleteFromQueue: checkUserIsLoginAndDeleteFromQueue
    };
};
