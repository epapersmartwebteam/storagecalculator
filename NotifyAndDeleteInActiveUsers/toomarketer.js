﻿var configs = require('./config');
var config = configs.toomarketerAPI;
var apiUrl = config[configs.env].url;


var async = require('async');
var _ = require('lodash');
var moment = require('moment');
var request = require('request');

var resultHelper = require('./resultHelper');

var log = require('./log');

module.exports = () => {
    function replaceMobile(mobile) {
        if (mobile) {
            return mobile.replace("+84", "0");
        }
        else {
            return '';
        }
    }
    function getToken(callback) {
        var url = apiUrl + 'api/authtoken';
        request(
            {
                uri: url,
                method: 'POST',
                encoding: 'utf-8',
                form: {
                    grant_type: 'password',
                    username: config.account.username,
                    password: config.account.password
                }
            },
            (err, resp, body) => {

                if (err) {
                    console.log(err);
                    callback(null);
                }
                else {
                    console.log(JSON.parse(body));
                    callback(JSON.parse(body).access_token);
                }
            });
    }

    function addContact(user, callback) {
        var url = apiUrl + 'api/Contact/Create';
        var form = {
            ContactGroupId: config.data.contactGroupId,
            ContactName: user.UserName,
            Email: user.Email,
            Mobile: replaceMobile(user.Mobile),
            BirthDate: user.Birthday,
            Gender: user.Gender,
            Note: user.Position,
            CustomFields: [
                {
                    CustomFieldId: config.data.customFields.userId.id,
                    CustomFieldName: config.data.customFields.userId.name,
                    CustomFieldValue: user.UserId
                },
                {
                    CustomFieldId: config.data.customFields.resend.id,
                    CustomFieldName: config.data.customFields.resend.name,
                    CustomFieldValue: ''
                },
                {
                    CustomFieldId: config.data.customFields.send.id,
                    CustomFieldName: config.data.customFields.send.name,
                    CustomFieldValue: 'true'
                }
            ]
        };

        getToken((token) => {
            request(
                {
                    uri: url,
                    method: 'POST',
                    encoding: 'utf-8',
                    headers: {
                        Authorization: 'bearer ' + token
                    },
                    form: form
                },
                (err, resp, body) => {
                    if (err) {
                        callback(resultHelper.returnResultServiceError(err));
                    }
                    else {
                        if (body) {
                            callback(JSON.parse(body));
                        }
                        else {
                            callback(resultHelper.returnResultServiceError({ statusCode: resp.statusCode, statusMessage: resp.statusMessage }));
                        }
                    }

                });
        });
    }

    function importContacts(lstUsers, isInActive, callback) {
        var url = apiUrl + 'api/Contact/CreateMany';

        var contacts = _.map(lstUsers, (user) => {
            return {
                ContactName: user.UserName,
                Email: user.Email,
                Mobile: replaceMobile(user.Mobile),
                BirthDate: user.Birthday,
                Gender: user.Gender,
                Note: user.Position,
                CustomFields: [
                    {
                        CustomFieldId: config.data.customFields.userId.id,
                        CustomFieldName: config.data.customFields.userId.name,
                        CustomFieldValue: user.UserId
                    },
                    {
                        CustomFieldId: config.data.customFields.resend.id,
                        CustomFieldName: config.data.customFields.resend.name,
                        CustomFieldValue: ''
                    },
                    {
                        CustomFieldId: config.data.customFields.send.id,
                        CustomFieldName: config.data.customFields.send.name,
                        CustomFieldValue: isInActive ? '' : 'true'
                    },
                    {
                        CustomFieldId: config.data.customFields.sendInActive.id,
                        CustomFieldName: config.data.customFields.sendInActive.name,
                        CustomFieldValue: isInActive ? 'true' : ''
                    }
                ]
            };
        });

        var form = {
            ContactGroupId: config.data.contactGroupId,
            ContactList: contacts,
            Note: '',
            CampaignId: config.data.campaignId
        };

        getToken((token) => {
            request(
                {
                    uri: url,
                    method: 'POST',
                    encoding: 'utf-8',
                    headers: {
                        Authorization: 'bearer ' + token
                    },
                    form: form
                },
                (err, resp, body) => {
                    if (err) {
                        callback(resultHelper.returnResultServiceError(err));
                    }
                    else {
                        if (body) {
                            callback(JSON.parse(body));
                        }
                        else {
                            callback(resultHelper.returnResultServiceError({ statusCode: resp.statusCode, statusMessage: resp.statusMessage }));
                        }
                    }
                });
        });
    }

    function deleteContacts(lstUserId, callback) {
        var url = apiUrl + 'api/Contact/DeleteMany';
        var form = lstUserId;

        getToken((token) => {
            request(
                {
                    uri: url,
                    method: 'POST',
                    encoding: 'utf-8',
                    headers: {
                        Authorization: 'bearer ' + token
                    },
                    form: form
                },
                (err, resp, body) => {
                    if (err) {
                        callback(resultHelper.returnResultServiceError(err));
                    }
                    else {
                        if (body) {
                            callback(JSON.parse(body));
                        }
                        else {
                            callback(resultHelper.returnResultServiceError({ statusCode: resp.statusCode, statusMessage: resp.statusMessage }));
                        }
                    }
                });
        });
    }

    function updateContactCustomFieldResend(token, userId, contactId, callback) {
        var url = apiUrl + 'api/Contact/UpdateContactCustomFields';

        var form = {
            ContactId: contactId,
            Active: true,
            CustomFields: [
                {
                    CustomFieldId: config.data.customFields.userId.id,
                    CustomFieldName: config.data.customFields.userId.name,
                    CustomFieldValue: userId
                },
                {
                    CustomFieldId: config.data.customFields.resend.id,
                    CustomFieldName: config.data.customFields.resend.name,
                    CustomFieldValue: 'true'
                }
            ]
        };

        request(
            {
                uri: url,
                method: 'POST',
                encoding: 'utf-8',
                headers: {
                    Authorization: 'bearer ' + token
                },
                form: form
            },
            (err, resp, body) => {
                if (err) {
                    callback(resultHelper.returnResultServiceError(err));
                }
                else {
                    if (body) {
                        callback(JSON.parse(body));
                    }
                    else {
                        callback(resultHelper.returnResultServiceError({ statusCode: resp.statusCode, statusMessage: resp.statusMessage }));
                    }
                }
            });
    }

    function updateContactCustomField(lstUser, callback) {


        getToken((token) => {
            async.eachLimit(lstUser, configs.rules.asynConcurrentLimit, (user, cb) => {
                updateContactCustomFieldResend(token, user.UserId, user.ContactId, () => {
                    cb();
                });
            },
                (err) => {
                    callback();
                });
        });
    }

    return {
        addContact: addContact,
        importContacts: importContacts,
        deleteContacts: deleteContacts,
        updateContactCustomField: updateContactCustomField
    };
};