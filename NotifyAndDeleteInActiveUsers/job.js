﻿var async = require('async');
var _ = require('lodash');
var moment = require('moment');

var config = require('./config');

var db = require('./db')();
var toomarketer = require('./toomarketer')();
var myXteamV1 = require('./myXteamV1')();
var log = require('./log');

var resultHelper = require('./resultHelper');
var errorCodes = resultHelper.errorCodes;

module.exports = () => {

    var scanDBGetUsers = function (isInActive, lastId, itemCount, callback) {
        if (isInActive) {
            console.log('scanDBGetInActiveUsers');
            scanDBGetInActiveUsers(lastId, itemCount, callback);
        }
        else {
            console.log('scanDBGetNotLoginUsers');
            scanDBGetNotLoginUsers(lastId, itemCount, callback);
        }
    };

    var scanDBGetNotLoginUsers = function (lastId, itemCount, callback) {
        db.getUserNotLoginOverNDays(config.rules.expireDay, lastId, itemCount, (err, lst) => {
            callback(lst);
        });
    };

    var scanDBGetInActiveUsers = function (lastId, itemCount, callback) {
        db.getUserInActiveOverNDays(config.rules.inActiveExpireDay, lastId, itemCount, (err, lst) => {
            callback(lst);
        });
    };

    var scanDBAndAddToQueue = function (isInActive, lastId) {
        var itemCount = config.rules.itemCount;
        scanDBGetUsers(isInActive, lastId, itemCount, (lst) => {
            log.info("IsInActive", isInActive, "LastId", lastId, "List length", lst.length);
            if (lst && lst.length > 0) {

                toomarketer.importContacts(lst, isInActive, (result) => {

                    if (result.ErrorCode == errorCodes.success) {

                        var contacts = result.Data;
                        log.info('Import success to Toomarketer ', contacts.length, ' contacts');
                        var lstUser = _.map(lst, (user) => {
                            return {
                                userId: user.UserId,
                                contactId: _.some(contacts, (contact) => {
                                    return _.some(contact.CustomFields, (cf) => {
                                        return cf.CustomFieldId == config.toomarketerAPI.data.customFields.userId.id && cf.CustomFieldValue == user.UserId;
                                    });
                                }) ? _.find(contacts, (contact) => {
                                    return _.some(contact.CustomFields, (cf) => {
                                        return cf.CustomFieldId == config.toomarketerAPI.data.customFields.userId.id && cf.CustomFieldValue == user.UserId;
                                    });
                                }).ContactId : null
                            };
                        });



                        db.addDeleteQueueUsers(lstUser, isInActive, (err, res) => {
                            if (err) {
                                log.error("Add to DeleteQueueUsers error", err);
                            }
                            if (lst.length == itemCount) {
                                lastId = _.last(lst).UserId;
                                setTimeout(() => {
                                    scanDBAndAddToQueue(isInActive, lastId);
                                }, 2000);
                            }
                            else {
                                log.info("Scan Finished");
                            }
                        });
                    }
                });
            }
        });
    };

    var scanDBAndUpdateToToomarketer = (lastId) => {
        var itemCount = config.rules.itemCount;
        db.getDeleteQueueUsers(false, config.rules.resendDay, lastId, itemCount, false, (err, lst) => {
            

            //hàm này chỉ update CustomField Resend cho phần ko đăng nhập, chứ ko resend cho Inactive
            toomarketer.updateContactCustomField(lst, () => {
                log.info('Finish Call to Toomarketer Resend');
                db.updateDeleteQueueResend(_.map(lst, (item) => {
                    return item.UserId;
                }), () => {
                    log.info('Update Resend to DB finished');
                });

                if (lst && lst.length >= itemCount) {
                    lastId = lst[lst.length - 1].UserId;
                    scanDBAndUpdateToToomarketer(lastId);
                }
            });
        });
    };

    var scanDBAndDeleteQueueUsers = (isInActive, lastId, deletedCount) => {
        var itemCount = config.rules.itemCount;
        var deleteDays = isInActive ? config.rules.inActiveDeleteDay : config.rules.deleteDay;

        db.getDeleteQueueUsers(isInActive, deleteDays, lastId, itemCount, null, (err, lst) => {

            async.eachLimit(lst,
                config.rules.asynConcurrentLimit,
                (user, cb) => {
                    var userId = parseInt(user.UserId);
                    log.info('userId', userId);
                    db.checkUserIsLoginAndDeleteFromQueue(userId, config.rules.expireDay, (canDelete) => {
                        if (canDelete) {
                            deleteAccount(userId, () => {
                                cb();
                            });
                        }
                        else {
                            log.info("Cancel Delete", userId);
                            cb();
                        }
                    })
                    
                    
                },
                (err) => {
                    log.info('Finished Delete DeleteQueueUsers ' + isInActive ? "InActive" : "");
                    if (!deletedCount) {
                        deletedCount = 0;
                    }
                    deletedCount += lst.length;

                    if (lst && lst.length >= itemCount && deletedCount < config.rules.maxDeletePerDay) {
                        lastId = lst[lst.length - 1].UserId;

                        scanDBAndDeleteQueueUsers(isInActive, lastId, deletedCount);
                    }
                });
        });
    };

    var deleteAccount = function (userId, callback) {
        log.warn('Starting Delete UserId', userId);
        myXteamV1.deleteAccount(userId, (result) => {
            if (result.errorCode == 0) {
                log.info("Delete UserId", userId, "success");
                db.removeDeleteQueueUser(userId, (res) => {
                    log.info("Remove From DeleteQueue", userId);
                    callback();
                });
            }
            else {
                log.error("Delete UserId", userId, "fail", result.error);
                callback();
            }
        });
    };

    return {
        scanDBAndAddToQueue: scanDBAndAddToQueue,
        scanDBAndUpdateToToomarketer: scanDBAndUpdateToToomarketer,
        scanDBAndDeleteQueueUsers: scanDBAndDeleteQueueUsers
    };
};