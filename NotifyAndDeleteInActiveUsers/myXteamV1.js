﻿var _ = require('lodash');
var configs = require('./config');

var config = configs.myXteamAPI[configs.env];

var request = require('request');

var resultHelper = require('./resultHelper');

module.exports = () => {
    var deleteAccount = function (userId, callback) {
        var url = config.url + 'user/deleteAccount';

        var form = {
            UserId: userId
        };

        request(
            {
                uri: url,
                method: 'POST',
                encoding: 'utf-8',
                headers: {
                    Authorization: 'bearer ' + config.botMXTV1Token
                },
                form: form
            },
            (err, resp, body) => {
                if (err) {
                    callback(resultHelper.returnResultServiceError(err));
                }
                else {
                    if (resp.statusCode == 200) {
                        callback(resultHelper.returnResultSuccess());
                    }
                    else {
                        callback(resultHelper.returnResultServiceError({ statusCode: resp.statusCode, statusMessage: resp.statusMessage }));
                    }
                }
            }
        );
    };

    return {
        deleteAccount: deleteAccount
    };
};