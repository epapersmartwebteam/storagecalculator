﻿var moment = require('moment');

module.exports = () => {

    var getUnixTimeStamp = () => {
        return parseInt(moment().utc().format('X'));
    };

    var convertDayToSecond = (days) => {
        return days * 24 * 60 * 60;
    };

    return {
        getUnixTimeStamp: getUnixTimeStamp,
        convertDayToSecond: convertDayToSecond
    };
};