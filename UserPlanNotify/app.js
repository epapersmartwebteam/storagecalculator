var async = require('async');
var _ = require('lodash');
var moment = require('moment');
var scheduler = require('node-schedule');

var calculateJobCron = '0 0 6 * * *';

var db = require('./db');
var log = require('./log');
var tooHelper = require('./tooHelper');
var notifyHelper = require('./notifyHelper');

var nextExpireDays = 18;
var asyncLimit = 10;
var notifyExpireTime1 = 15;
var notifyExpireTime2 = 1;

function notifyToUser(mU) {
    if (mU.Email) {
        tooHelper.sendEmailExpire(mU, () => {
        });
    }

    if (mU.Mobile) {
        tooHelper.sendSMSExpire(mU, () => {
        });
    }

    notifyHelper.sendNotify(mU.UserId, mU.CurrentPlanName, moment.unix(mU.EndDateUnix).format('YYYY-MM-DD'));
}

function runCheckPlans() {
    db.getUsersToCheckPlan(nextExpireDays, function (err, lst) {
        log.info(lst.length + ' users need to check');
        async.eachLimit(lst, asyncLimit, (user, cb) => {
            var dayBeforeExpire = moment.unix(user.EndDateUnix).startOf('day').diff(moment().utc().startOf('day'), 'days');
            var message = user.UserId + ' - ' + user.UserName + ' - ' + user.CurrentPlanName + ' - ' + moment.unix(user.EndDateUnix).format('YYYY-MM-DD');
            var needNotify = false;
            if (dayBeforeExpire == notifyExpireTime1 || dayBeforeExpire == notifyExpireTime2) {
                notifyToUser(user);
                needNotify = true;
            }

            message += needNotify ? ' - send notify' : ' - ignore';
            log.info(message);
            cb();
        },
            (err) => {
                log.info('Check user plan done');
            }
        );
    });
}

setTimeout(() => {
    //tooHelper.sendSMSExpire({ UserId: 1, UserName: 'H?ng L�', Email: 'hung.le@epapersmart.com', Mobile: '0943282606', CurrentPlanName: 'G�i Platinum', EndDate: '2018-11-20' },
    //    (err, res) => { console.log(res); });
    //notifyToUser({ UserId: 1, UserName: 'H?ng L�', Email: 'hung.le@epapersmart.com', Mobile: '0943282606', CurrentPlanName: 'G�i Platinum', EndDate: '2018-11-20' });
    scheduler.scheduleJob(calculateJobCron, function (fireDate) {
        log.info('User Plan checker was supposed to run at ' + fireDate + ', but actually ran at ' + new Date());

        runCheckPlans();
    });
}, 1000);
