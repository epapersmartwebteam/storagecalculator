﻿var db = require('./db');
var async = require('async');
var _ = require('lodash');
var moment = require('moment');
var request = require('request');

var mailTemplate = 'PlanExpireNotify';


var apiUrl = 'http://api.toomarketer.com/';
var apiUserName = 'support@myxteam.com';
var apiPassword = 'qazwsxcv';
var fromName = 'MyXteam';
var fromEmail = 'support@myxteam.com';
var brandNameSMSTypeId = 2;
var myXteamBrandNameId = 126;
var myXteamBrandName = 'MYXTEAM';

function getToken(callback) {
    var url = apiUrl + 'api/authtoken';
    request(
        {
            uri: url,
            method: 'POST',
            encoding: 'utf-8',
            form: { grant_type: 'password', username: apiUserName, password: apiPassword }
        },
        (err, resp, body) => {
            if (err) {
                callback(null);
            }
            else {
                callback(JSON.parse(body).access_token);
            }
        });
}

function sendEmail(fromName, fromEmail, subject, html, toMail, toName, callback) {
    getToken((token) => {
        var url = apiUrl + 'api/EmailSend/Send';

        var form = {
            Subject: subject,
            FromEmail: fromEmail,
            ReplyToEmail: fromEmail,
            FromName: fromName,
            HTML: html,
            ShowPermissionReminder: false,
            ShowUnsubscribeButton: false,
            EmbedMailUrl: false,
            Facebook: '',
            Twitter: '',
            ToEmail: toMail,
            ToName: toName
        };

        request(
            {
                uri: url,
                method: 'POST',
                encoding: 'utf-8',
                headers: {
                    Authorization: 'bearer ' + token
                },
                form: form

            },
            (err, resp, body) => {
                callback(err, body);
            });
    });
}

function sendEmailExpire(mU, callback) {
    var template = '';
    var webLink = '';
    var lstOS = [];

    var tasks = [];
    tasks.push((cb) => {
        db.getMailTemplate(mailTemplate, (err, res) => {
            if (!err) {
                template = res;
            }
            cb();
        });
    });

    tasks.push((cb) => {
        db.getSettingValue('website_url', (err, res) => {
            if (!err) {
                webLink = res;
            }
            cb();
        });
    });

    tasks.push((cb) => {
        db.getOSList((err, res) => {
            if (!err) {
                lstOS = res;
            }
            cb();
        });
    });

    async.parallel(tasks, (err) => {
        if (!err) {
            var mailBody = template.Message;
            mailBody = mailBody.replace("[WEBLINK]", webLink);
            console.log(template);
            console.log(webLink);
            console.log(lstOS);
            mailBody = mailBody.replace("[ANDROIDLINK]", _.find(lstOS, (s) => { return s.OS == "Android" }).AppLink);
            mailBody = mailBody.replace("[IOSLINK]", _.find(lstOS, (s) => { return s.OS == "iOS" }).AppLink);

            var message = "Gói đăng ký <b>" + mU.CurrentPlanName + "</b> của bạn sắp hết hạn sử dụng (ngày hết hạn: " + moment(mU.EndDate).format("YYYY-MM-DD") + ").<br />";
            message += "Bạn vui lòng gia hạn gói trước thời hạn trên để không bị gián đoạn trong việc sử dụng. <br />";
            message += "Bạn có thể bấm vào <a href='https://app.myxteam.com/Account/?v=SubscriptionPlan'>link này</a> để gia hạn ngay gói sử dụng.";

            mailBody = mailBody.replace("[MESSAGE]", message);

            sendEmail(fromName, fromEmail, template.Subject, mailBody, mU.Email, mU.UserName, (err, res) => {
                callback(err, res);
            });
        }
        else {
            callback(err);
        }
    });
}
exports.sendEmailExpire = sendEmailExpire;

function sendSMS(mobileNumber, message, sMSTypeId, brandNameId, brandName, callback) {
    getToken((token) => {
        var url = apiUrl + 'api/SmsSend/SendOTP';
        var length = message.length;
        var messageLength = 1;

        var form = {
            MobileNumber: mobileNumber,
            Message: message,
            MessageLength: messageLength,
            SMSTypeId: sMSTypeId,
            BrandNameId: brandNameId,
            BrandName: brandName
        };
        request(
            {
                uri: url,
                method: 'POST',
                headers: {
                    Authorization: 'bearer ' + token
                },
                form: form

            },
            (err, resp, body) => {
                callback(err, body);
            });
    });
}
function sendSMSExpire(mU, callback) {
    var message = "Goi su dung tai khoan cua QK sap het han (" + moment.unix(mU.EndDateUnix).format("DD/MM/YYYY") + "). Vui long gia han de khong bi gian doan khi su dung.";
    sendSMS(mU.Mobile, message, brandNameSMSTypeId, myXteamBrandNameId, myXteamBrandName, (err, res) => {
        callback(err, res);
    });
}
exports.sendSMSExpire = sendSMSExpire;