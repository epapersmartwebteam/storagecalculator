﻿var db = require('./db');
var async = require('async');
var _ = require('lodash');
var moment = require('moment');
var soap = require('soap');

var apiUrl = 'http://sns.myxteam.com/SNS.svc?wsdl';

var curClient;
soap.createClient(apiUrl, function (err, client) {
    curClient = client;
});

function sendNotify(userId, currentPlan, endDate) {
    curClient.UserPlanExpireNotifyAsync({UserId: userId, CurrentPlan: currentPlan, EndDate: endDate });
}

exports.sendNotify = sendNotify;