﻿var sql = require('mssql');

var config = {
    user: 'adminsrv',
    password: 'P@12345678vm',
    server: '119.82.135.46', // You can use 'localhost\\instance' to connect to named instance
    database: 'myxteam'
};

const connection = new sql.ConnectionPool(config, err => {
});

connection.on('error', function (err) {
    console.log('connection error');
    console.log(err);
    connection = new sql.ConnectionPool(config);
    connection.connect();
});

function getUsersToCheckPlan(nextExpireDays, callback) {
    var request = new sql.Request(connection);
    request.input('NextExpireDays', sql.Int, nextExpireDays);

    request.execute('getUsersForPlanCheckNotifyUnix', (err, result) => {
        if (err) {
            callback(err);
        }
        else {
            callback(null, result.recordset);
        }
    });
}
exports.getUsersToCheckPlan = getUsersToCheckPlan;

function getMailTemplate(templateKey, callback) {
    var request = new sql.Request(connection);
    var query = "Select * From EmailTemplates Where TemplateKey = N'" + templateKey + "'";
    request.query(query, (err, result) => {
        if (err) {
            callback(err);
        }
        else {
            var template = (result.recordset && result.recordset.length > 0) ? result.recordset[0] : '';
            callback(null, template);
        }
    });
}
exports.getMailTemplate = getMailTemplate;

function getSettingValue(settingKey, callback) {
    var request = new sql.Request(connection);
    var query = "Select * From Settings Where SetingKey = N'" + settingKey + "'";
    request.query(query, (err, result) => {
        if (err) {
            callback(err);
        }
        else {
            var settingValue = (result.recordset && result.recordset.length > 0) ? result.recordset[0].SettingValue : '';
            callback(null, settingValue);
        }
    });
}
exports.getSettingValue = getSettingValue;

function getOSList(callback) {
    var request = new sql.Request(connection);
    var query = "Select * From OSSetting";
    request.query(query, (err, result) => {
        if (err) {
            callback(err);
        }
        else {            
            callback(null, result.recordset);
        }
    });
}
exports.getOSList = getOSList;